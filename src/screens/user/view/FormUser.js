import React from 'react';
import { Modal, Input } from 'antd';


class FormUser extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: ''
        }
    }
    handleOk = (e) => {
        this.props.createUser({name: this.state.name, email: this.state.email})
    }

    handleCancel = (e) => {
        this.props.openForm()
    }
    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        const { isOpenForm } = this.props
        const { name, email } = this.state
        return (
            <Modal
                title="Basic Modal"
                visible={isOpenForm}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
            >
                Name: <Input value={name} placeholder="name" onChange={this.handleInput} name="name" />
                <br />
                Email :<Input value={email} placeholder="email" onChange={this.handleInput} name="email" />
            </Modal>
        )
    }
}

export default FormUser