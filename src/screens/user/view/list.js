import React from 'react';
import { Divider, Tag, Button } from 'antd';
import { connect } from 'react-redux';
import userAction from '../models/redux/action';
import { Table } from '../../../components';
import FormUser from './FormUser';


const data = [];

class ListUser extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpenForm: false
        }
    }
    componentDidMount() {
        this.props.getInfos()
    }
    getInfos = () => {

    }

    deleteUser = (id) => {
        this.props.deleteInfo({ id })
    }

    renderColumns = () => {
        return [
            {
                title: 'STT',
                dataIndex: 'id',
                key: 'id',
                render: (text) => <p>{text}</p>

            },
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
                render: text => <a href="javascript:;">{text}</a>,
            }, {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <a href="javascript:;">Edit</a>
                        <Divider type="vertical" />
                        <a href="javascript:;" onClick={() => this.deleteUser(record.id)}>Delete</a>
                    </span>
                ),
            }]

    }
    openForm = () => {
        this.setState({
            isOpenForm: !this.state.isOpenForm
        })
    }

    createUser = (data) => {
        console.log('data: ', data)
        this.props.addInfo(data)
    }
    render() {
        const { infos, isProcessing } = this.props
        const { isOpenForm } = this.state
        return (
            <div>
                <Button type="primary" onClick={this.openForm}>Primary</Button>
                <Table
                    data={infos}
                    columns={this.renderColumns()}
                    loading={isProcessing}
                    title={() => 'List of users'}
                // footer={() => 'Footer'}
                />
                <FormUser
                    isOpenForm={isOpenForm}
                    openForm={this.openForm}
                    createUser={(data) =>this.createUser(data)}
                />
                <h1>API error</h1>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        infos: state.user.data,
        isProcessing: state.user.isProcessing
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        getInfos: (payload) => dispatch(userAction.getUsers(payload)),
        deleteInfo: (payload) => dispatch(userAction.deleteUser(payload)),
        addInfo: (payload) => dispatch(userAction.createUser(payload))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListUser)