import { ApiConst } from '../../../../configs';
import { request } from '../../../../utils';
// Export
export default {
    /**
     * service list user
     * @param {Object} payload
     */
    getUsers: async (payload) => {
        let result = {
            error: null,
            data: []
        }
        const api = ApiConst.user.getUsers()
        const options = {
            method: api.method,
            ...payload
        }
        const response = await request(api.url, options)
        if (!response.messageError) {
            result.data = response.data
        } else { // error system
            result.error = response.messageError
        }
        return result
    },
    /**
     * service delete user
     * @param {Object} payload
     */
    deleteUser: async (payload) => {
        let result = {
            error: null,
            data: []
        }
        const api = ApiConst.user.deleteUser()
        const options = {
            method: api.method,
            ...payload
        }
        const response = await request(api.url, options)
        console.log('response: ', response)
        if (!response.messageError) {
            result.data = response.data
        } else { // error system
            result.error = response.messageError
        }
        return result
    },
    /**
     * service add user
     * @param {Object} payload
     */
    createUser: async (payload) => {
        let result = {
            error: null,
            data: []
        }
        const api = ApiConst.user.createUser()
        const options = {
            method: api.method,
            ...payload
        }
        const response = await request(api.url, options)
        if (!response.messageError) {
            result.data = response.data
        } else { // error system
            result.error = response.messageError
        }
        return result
    },
}