import { put, call } from 'redux-saga/effects';
import userAction from '../redux/action';

export default {
   /**
   *  saga list user
   * @param {Object} api service
   * @param {Object} payload object to pass api
   */
    *getUsers(api, payload) {
        try {
            let data = yield call(api, payload)
            yield put(userAction.getUsersResponse(data))
        } catch (error) {
            console.log('error list: ', error)
        }
    },

   /**
   *  saga list user
   * @param {Object} api service
   * @param {Object} payload object to pass api
   */
    *deleteUser(api, payload) {
        try {
            let data = yield call(api, payload)
            console.log('data after delete: ', data)
            yield put(userAction.deleteUserResponse(data))
        }
        catch (error) {
            console.log('error delete: ', error)
        }
    },
   /**
   *  saga add user
   * @param {Object} api service
   * @param {Object} payload object to pass api
   */
    *createUser(api, payload) {
        try {
            let data = yield call(api, payload)
            yield put(userAction.createUserResponse(data))
        }
        catch (error) {
            console.log('error delete: ', error)
        }
    },

}