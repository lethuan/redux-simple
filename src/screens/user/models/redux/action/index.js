import { userActionTypes } from '../../../../../actions/actionTypes';

/**
 * Info creators
 * 
 * @param {Object} payload
 */
const getUsers = payload => ({
    type: userActionTypes.getUsers,
    payload
})

const getUsersResponse = payload => ({
    type: userActionTypes.getUsersResponse,
    payload
})


/**
 * Info creators
 * 
 * @param {Object} payload
 */
const getUser = payload => ({
    type: userActionTypes.getUser,
    payload
})

const deleteUser = (payload) => ({
    type: userActionTypes.deleteUser,
    payload
})

const deleteUserResponse = (payload) => ({
    type: userActionTypes.deleteUserResponse,
    payload
})

const createUser = (payload) => ({
    type: userActionTypes.createUser,
    payload
})

const createUserResponse = (payload) => ({
    type: userActionTypes.createUserResponse,
    payload
})

/**
 * Export action
 */
const userAction = {
    getUsers,
    getUser,
    getUsersResponse,
    deleteUser,
    deleteUserResponse,
    createUser,
    createUserResponse
}

export default userAction