import Immutable from 'seamless-immutable';
import { userActionTypes } from '../../../../../actions/actionTypes';

const INIT_STATE = Immutable({
    data: [],
    isProcessing: false
})
/**
 * Info reducers
 */
const userReducer = (state = INIT_STATE, action) => {
    const { payload } = action
    switch (action.type) {
        case userActionTypes.getUsers:
            return {
                data: [
                    ...state.data
                ],
                isProcessing: true
            }
        case userActionTypes.getUsersResponse:
            return {
                ...state,
                data: payload.data,
                isProcessing: false,
            }
        case userActionTypes.createUserResponse:
        console.log('payload: ', payload)
        console.log('state:', state)
            return {
                ...state,
                data: [...payload.data],
                isProcessing: false,
            }
        case userActionTypes.deleteUserResponse:
            return {
                ...state,
                data: state.data.filter(item => item.id !== payload.data.id)
            }
        default:
            return state
    }

}


// export user reducers
export {
    userReducer
}