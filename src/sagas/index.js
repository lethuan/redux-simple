import { takeLatest, all } from 'redux-saga/effects';
import { userAction, userSaga, userService } from '../screens/user/models'


export default function* rootSaga() {
    yield all([
        takeLatest(userAction.getUsers().type, userSaga.getUsers, userService.getUsers),
        takeLatest(userAction.deleteUser().type, userSaga.deleteUser, userService.deleteUser),
        takeLatest(userAction.createUser().type, userSaga.createUser, userService.createUser),
    ])
}
 