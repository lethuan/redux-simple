import { combineReducers } from 'redux'
import { userReducer } from '../screens/user/models/redux/reducer'

export default combineReducers({
    user: userReducer,

})