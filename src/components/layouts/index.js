import React from 'react'
import MenuBar from './MenuBar'

class LayoutView extends React.Component {
  render() {
    return (
      <MenuBar>
        { this.props.children }
      </MenuBar>
    )
  }
}
export default LayoutView
