import React from 'react'
import { Layout, Menu, Icon, Button } from 'antd'
import { Link } from 'react-router-dom'
import ProfileView from './ProfileUser.js'
import './style.less'

const { Header, Sider, Content } = Layout;

export default class MenuBar extends React.Component {
  state = {
    collapsed: true,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  render() {
    return (
      <Layout
        style={{
        }}
      >
        <Sider
          // trigger={null}
          // collapsible
          collapsed={this.state.collapsed}
        >
          <div className="logo" />
          <Menu theme="dark" mode="inline">
            <Menu.Item key="1">
              <Link to="/products">
                <Icon type="shop" />
                <span>Product</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/categories">
                <Icon type="appstore" />
                <span>Category</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/users">
                <Icon type="user" />
                <span>User</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="4">
              <Link to="/roles">
                <Icon type="key" />
                <span>Role</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: '20px', display: 'flex', justifyContent: 'space-between' }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
            <ProfileView
              
            />
          </Header>
          <Content style={{ margin: '24px 16px', padding: 24, background: '#fff' }}>
            {this.props.children}
          </Content>
        </Layout>
      </Layout>
    );
  }
}
