import React from 'react'
import { Icon } from 'antd'
import { withRouter } from 'react-router-dom';
// import { resetLocalStorage } from '../../utils/localStorage'

class Index extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  /**
   * remove all storage and logout
   */
  logout = () => {
    // resetLocalStorage()
    this.props.history.push('/login', { isLogin: false })
  }

  render() {
    return (
      <div
        style={{
          alignSelf: 'center',
          cursor: 'pointer'
        }}
        onClick={() => this.logout()}
      >
        <span>Logout </span>
        <Icon
          type="logout"
        />
      </div>
    )
  }
}
export default withRouter(Index)
