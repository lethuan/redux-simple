import React from 'react';
import { Table } from 'antd';

export default ({ data = [], columns, loading = false, title, footer, textEmpty = 'No data' }) => {
    return (
        <Table
            columns={columns}
            dataSource={data}
            loading={loading}
            title={title}
            footer={footer}
        />
    )
}