/**
 * Type of user action 
 */
const userActionTypes = {
    getUsers: 'GET_USERS',
    getUsersResponse: 'GET_USER_RESPONSE',
    getUser: 'GET_USERS',
    createUser: 'CREATE_USERS',
    createUserResponse: 'CREATE_USERS_RESPONSE',
    deleteUser: 'DELETE_USERS',
    deleteUserResponse: 'DELETE_USERS_RESPONSE',
}

// export
export {
    userActionTypes
}