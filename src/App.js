import React from 'react';
import { Provider } from 'react-redux';
import 'antd/dist/antd.css';
import { Page404} from './screens/errors';
import { LayoutView } from './components';
import LoginView from './screens/login'
import store from './store/configureStore';
import routes from './routes';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';

/**
 * 
 * @param {Component} Component specific component render
 * @param {rest} rest the rest properties
 */
const AdminRoute = ({ Component, ...rest }) => (
  <Route
    {...rest}
    render={
      props => (
        // isAuthenticated() ?
        true ?
          (
            <LayoutView>
              <Component {...props} />
            </LayoutView>
          )
          :
          <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
      )
    }
  />
)

const HomePage = () => (
  <React.Fragment>
    <Provider store={store}>
      <Router>
        <Switch>
          {
            routes && routes.map(route => (
              <AdminRoute
                key={route.path}
                path={route.path}
                Component={route.component}
              />
            ))
          }
          <Route exact path='/' component={LoginView} />
          <Route exact path='/login' component={LoginView} />
          <Route exact path='*' component={Page404} />

        </Switch>
      </Router>
    </Provider>
  </React.Fragment>

)

export default HomePage
