import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga' 
import rootReducer from '../reducers';
import rootSaga from '../sagas'
// import { createLogger } from 'redux-logger';
// const loggerMiddleware = createLogger()
const sagaMiddleware = createSagaMiddleware()
const configureStore = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware)
)
sagaMiddleware.run(rootSaga)

// export
export default configureStore
