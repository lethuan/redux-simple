
const ENDPOINT = {
  dev: 'http://api.fimobits.cobonla.org/admin',
  pro: ''
}

const METHODS = {
  get: 'get',
  post: 'post',
  put: 'put',
  path: 'path',
  delete: 'delete',
}


export default {
  endPoint: ENDPOINT.dev,
  METHODS,
  RESPONSE: {
    STATUS: {
      success: 200
    }
  },
  login: {
    getLogin: () => {
      return {
        url: '/auth/login',
        method: METHODS.post
      }
    }
  },
  user: {
    getUsers: () => {
      return {
        url: '/users',
        method: METHODS.get
      }
    },
    getUser: (id) => {
      return {
        url: `user/detail`,
        method: METHODS.get
      }
    },
    createUser: () => {
      return {
        url: 'users',
        method: METHODS.post
      }
    },
    deleteUser: () => {
      return {
        url: 'users/',
        method: METHODS.delete
      }
    },
    updateUser: () => {
      return {
        url: 'user/update',
        method: METHODS.post
      }
    },
    
  },
  role: {
    getRoles: () => {
      return {
        url: '/role/list',
        method: METHODS.get
      }
    }
  }
}
