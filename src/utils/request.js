import axios from 'axios';
import ApiConfigs from '../configs/api'

const instance = axios.create({
    // baseURL: 'http://api.fimobits.cobonla.org/admin',
    // baseURL: 'https://5b49c211ff11b100149bf410.mockapi.io',
    baseURL: 'https:://5a0e9d5db69ef50012be3690.mockapi.io',
    timeout: 3000,
    headers: {
        'Cache-Control': 'no-cache',
        'Content-Type': 'application/json'
    },
})
/**
 * 
 * @param {*} url 
 * @param {*} options 
 */
const request = async (url, options) => {
    let result = {
        messageError: null,
        data: null
    }

    const { method } = options || 'get'
    let response = {}
    if(method === ApiConfigs.METHODS.get) {
        response = await instance[method](url)
    }

    if(method === ApiConfigs.METHODS.post) {
        response = await instance[method](url)
    }

    if (method === ApiConfigs.METHODS.delete) {
        const { payload }  = options
        let urlDelete=`${url}/${payload.id}`
        response = await instance[method](urlDelete)
    }
    if ((response.status === 200  && response.statusText === 'OK') || (response.status === 201  && response.statusText === 'Created')) { // response success
        result.data = response.data
    } else { // error system
        result.messageError = 'System error !'
    }

    return result
}

// export
export {
    request
}